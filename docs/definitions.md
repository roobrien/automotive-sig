# Automotive SIG Vocabulary

This page defines terms used by the Automotive SIG that
might be new to the reader or subject to different interpretation.

If something is not clear, let us know.

## CentOS Stream

CentOS Stream is a continuously delivered distribution that tracks just ahead
of Red Hat Enterprise Linux (RHEL) development, positioned as a midstream 
between Fedora and RHEL.

## OSBuild manifests

OSBuild manifests are JSON files that instruct [OSBuild](https://www.osbuild.org/)
on how to build images.

## Special Interest Group (SIG)

A SIG is a group of people who are interested in the same goal
and willing to collaborate to move objectives that support the goal further.
The CentOS Automotive SIG is a group of people who are interested
in developing an operating system (OS) based on CentOS Stream that could be used
in an automotive environment. This OS will not be safety certified, however, and
should be considered as a research project or a proof of concept (POC).
