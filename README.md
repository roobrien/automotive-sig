# Automotive SIG

This is the central repository for the Automotive SIG. It contains recipes for
building the images, information on how to download them, and
documentation.

For more information, visit:

* [Automotive SIG wiki page] (https://wiki.centos.org/SpecialInterestGroup/Automotive)
* [Automotive SIG documentation] (https://sigs.centos.org/automotive)
